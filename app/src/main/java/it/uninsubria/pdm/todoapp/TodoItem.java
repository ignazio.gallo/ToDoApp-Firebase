package it.uninsubria.pdm.todoapp;


import com.google.firebase.database.Exclude;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by ignazio on 3/15/18.
 */

public class TodoItem {
    private String todo;  // il task
    private GregorianCalendar createdOn; // la data di creazione del task
    private String id;

    public TodoItem(String todo) {
        super();
        this.todo = todo;
        setCreatedOn(new GregorianCalendar());
    }

    public TodoItem() {
        super();
    }

    @Override
    public String toString() {
        String currentDate = new
                SimpleDateFormat("dd/MM/yyyy").format(createdOn.getTime());
        return currentDate + ":\n >> " + todo;
    }

    @Exclude
    public GregorianCalendar getCreatedOn() {
        return createdOn;
    }

    public String getCreatedOnString() {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-SSSXXX");
        return dt.format(createdOn.getTime());
    }

    public String getTodo() {
        return todo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTodo(String task) {
        this.todo = task;
    }

    @Exclude
    public void setCreatedOn(GregorianCalendar date) {
        this.createdOn = date;
        id = getCreatedOnString();
    }

    public void setCreatedOnString(String dateString) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-SSSXXX");
        Date date = null;
        try {
            date = df.parse(dateString);
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            setCreatedOn(gc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }
}
