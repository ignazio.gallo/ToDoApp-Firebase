package it.uninsubria.pdm.todoapp;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public static final String FB_TODO_USERS = "users";
    public static final String FB_TODO_ITEMS = "items";
    private static final String TAG = "MainActivity";

    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;
    private String mUserId;

    private ArrayList<TodoItem> todoItems;

    private LinearLayoutManager mLinearLayoutManager;
    private GridLayoutManager mGridLayoutManager;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            loadLogInView();
        }
        mUserId = mFirebaseUser.getUid();

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);

        // Display icon in the toolbar
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        // the array list containing the todo items
        todoItems = new ArrayList<>();

        mRecyclerView = findViewById(R.id.recyclerView);
        //mRecyclerView.setLayoutManager(mLinearLayoutManager);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            mLinearLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLinearLayoutManager);
        }
        else{
            mGridLayoutManager = new GridLayoutManager(this, 2);
            mRecyclerView.setLayoutManager(mGridLayoutManager);
        }

        mAdapter = new RecyclerAdapter(todoItems);
        mRecyclerView.setAdapter(mAdapter);

        // This will attach the ItemTouchListener to the RecyclerView
        setRecyclerViewItemTouchListener();

        // App Open event. By logging this event when an App is moved to the foreground,
        // developers can understand how often users leave and return during the course of a Session.
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, null);

        // Use Firebase to populate the list.
        mDatabase.child(FB_TODO_USERS).child(mUserId).child(FB_TODO_ITEMS).addChildEventListener(
                new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String,Object> map = (Map<String, Object>) dataSnapshot.getValue();
                TodoItem item = new TodoItem();
                item.setId((String) map.get("id"));
                item.setCreatedOnString((String) map.get("createdOnString"));
                item.setTodo((String) map.get("todo"));
                todoItems.add(0, item);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onChildAdded added " + item);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {           }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {            }

            @Override
            public void onCancelled(DatabaseError databaseError) {            }
        });
    }


    // executes onDataChange method immediately and after executing that method once,
    // it stops listening to the reference location it is attached to.
    private void readAllItemsFromFirebase() {
        DatabaseReference ref = mDatabase.child(FB_TODO_USERS).child(mUserId).child(FB_TODO_ITEMS);
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        todoItems.clear();
                        //Get map of items in datasnapshot
                        for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                            TodoItem todo = messageSnapshot.getValue(TodoItem.class);
                            //System.out.println(todo);
                            todoItems.add(todo);
                            Log.d(TAG, "onDataChange added " + todo);
                        }
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });
    }


    private void loadLogInView() {
        Intent intent = new Intent(this, LogInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    static final int NEW_ITEM_REQUEST = 1;  // The request code

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(),
                    "Not yet implemented!",
                    Toast.LENGTH_SHORT).show();
            return true;
        } else if (id == R.id.action_new_item) {
            Intent i = new Intent(getApplicationContext(), AddNewItemActivity.class);
            startActivityForResult(i, NEW_ITEM_REQUEST);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == NEW_ITEM_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String returnValue = data.getStringExtra("TODO_TASK");
                Log.d(MainActivity.class.getName(), "onActivityResult() -> " + returnValue);
                addNewItem(returnValue);
                return;
            }
        }
    }

    private void addNewItem(String todo) {
        if (todo.length() == 0) {
            Toast.makeText(getApplicationContext(),
                    "Empty ToDo string",
                    Toast.LENGTH_LONG).show();
            return;
        }
        TodoItem newTodo = new TodoItem(todo);
        mDatabase.child(FB_TODO_USERS).child(mUserId).child(FB_TODO_ITEMS).child(newTodo.getId()).setValue(newTodo);
    }


    private void setRecyclerViewItemTouchListener() {
        // 1 - You create the callback and tell it what events to listen for.
        // It takes two parameters, one for drag directions and one for swipe directions, but
        // you’re only interested in swipe, so you pass 0 to inform the callback not to respond to drag events.
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
        {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder viewHolder1)      {
                // 2 - You return false in onMove because you don’t want to perform any special behavior here.
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                // 3 - onSwiped is called when you swipe an item in the direction specified in the
                // ItemTouchHelper. Here, you request the viewHolder parameter passed for the
                // position of the item view, then you remove that item from your list of photos.
                // Finally, you inform the RecyclerView adapter that an item has been removed at a
                // specific position.
                int position = viewHolder.getAdapterPosition();
                TodoItem item = todoItems.remove(position);
                // delete the item from the DB
                DatabaseReference ref = mDatabase.child(FB_TODO_USERS).child(mUserId).child(FB_TODO_ITEMS);
                ref.child(item.getId()).removeValue();

                mRecyclerView.getAdapter().notifyItemRemoved(position);

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, ""+item.getId());
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Delete_New_ToDoItem");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "string");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            }
        };

        // 4 - You initialize the ItemTouchHelper with the callback behavior you defined, and
        // then attach it to the RecyclerView.
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

}
